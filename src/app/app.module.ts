import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { HeaderModule } from './header/header.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { NgxPopper } from 'angular-popper';

 import { ServicesAliexpressService } from './aliexpress/services-aliexpress.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HeaderModule,
    BrowserModule,
    AppRoutingModule,
    NgxPopper,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    ServicesAliexpressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
