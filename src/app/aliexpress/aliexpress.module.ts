import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AliExpressDashboardComponent } from './ali-express-dashboard/ali-express-dashboard.component';
import { AliExpressSearchComponent } from './aliexpress-search/aliexpress-search.component';
import { CategorySelectComponent } from './category-select/category-select.component';

import { ServicesAliexpressService } from './services-aliexpress.service';
import { ControlsComponent } from './controls/controls.component';
import { ViewProductsComponent } from './view-products/view-products.component';
import { ViewProductComponent } from './view-product/view-product.component';

const routes: Routes = [
  {path: '', component: AliExpressDashboardComponent},
  {path: 'category/:categoryId', component: ViewProductsComponent },
  {path: 'product/:productId', component: ViewProductComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AliExpressDashboardComponent, AliExpressSearchComponent, CategorySelectComponent, ControlsComponent, ViewProductsComponent, ViewProductComponent],
  exports: [
    AliExpressDashboardComponent
  ],
  providers: [
    ServicesAliexpressService
  ]
})
export class AliExpressModule{}
