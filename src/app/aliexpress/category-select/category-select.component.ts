import { Component, OnInit } from '@angular/core';


import { ServicesAliexpressService } from '../services-aliexpress.service';
@Component({
  selector: 'app-aliexpress-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.css']
})
export class CategorySelectComponent implements OnInit {

  aliExpressCategories: Array<object>;
  //inject service

  constructor(service: ServicesAliexpressService) {
    let categoryPull = service.getMainProductCategories();
    categoryPull.subscribe( (result: Array<object>) => {
        console.log(result)
        this.aliExpressCategories = result;
    })
    if(this.aliExpressCategories == null){
      this.aliExpressCategories = [
        {categoryName:"Snakes", categoryId: "1", subcategories:[
          {categoryName: "SnakesSub", categoryId:"1.1"},
          {categoryName: "SnakesSub", categoryId:"1.2"},
          {categoryName: "SnakesSub", categoryId:"1.3"},
          {categoryName: "SnakesSub", categoryId:"1.4"},
        ]},
        {categoryName:"Snakes2", categoryId: "2"},
        {categoryName:"Snakes3", categoryId: "3"}
      ]
    }
  }
  ngOnInit() {
  }
  clicked($event){
    console.log($event.target.id);
  }
}
