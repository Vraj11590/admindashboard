import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  productData: Array<object>;

  ngOnInit() {
      console.log("grab url params stuff..");
  }




  constructor() {
    this.productData = [{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    }]
  }


}
