import { Component } from '@angular/core';




import { ServicesAliexpressService } from '../services-aliexpress.service';

@Component({
  selector: 'app-ali-express-dashboard',
  templateUrl: './ali-express-dashboard.component.html',
  styleUrls: ['./ali-express-dashboard.component.css']
})
export class AliExpressDashboardComponent {

  category: Array<object>;
  productCategories: Array<object>;
  rows: number;

  constructor(service: ServicesAliexpressService){
    let categoryPull = service.getMainProductCategories();
    categoryPull.subscribe( (result: Array<object>) => {
        console.log(result)
        this.productCategories = result;
    })
    if (this.productCategories == null){
      this.productCategories = [
          {categoryName: "Kitchen", categoryId: 13, totalProducts: 100, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Tables", categoryId: 14, totalProducts: 200, lastFetched: "Yesterday", lastPushed: "Never" },
          {categoryName: "Computer", categoryId: 15, totalProducts: 300, lastFetched: "4h ago", lastPushed: "3h ago"},
          {categoryName: "Test4", categoryId: 16, totalProducts: 400, lastFetched: "20m ago", lastPushed: "20m ago"},
          {categoryName: "Test5", categoryId: 17, totalProducts: 500, lastFetched: "6h ago", lastPushed: "6h ago"},
          {categoryName: "Test6", categoryId: 18, totalProducts: 600, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test7", categoryId: 19, totalProducts: 700, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test8", categoryId: 20, totalProducts: 800, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test9", categoryId: 21, totalProducts: 900, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test10", categoryId: 22, totalProducts:1000, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test11", categoryId: 23, totalProducts:1100, lastFetched: "Never", lastPushed: "Never"}
      ]
  }
  }
  ngOnInit(){
    console.log("init ali-express dashboard");
  }
  firePush(object, service: ServicesAliexpressService){
    console.log(object)
    let wantedPush = object.target.id;
    let categoryName = object.srcElement.parentElement.parentNode.cells[0].innerText; // i dont even need to find anything else is this they best way of doing it?
    console.log("Firing Push for Object " + "id: " + wantedPush + ", categoryName: " + categoryName);
    console.log(categoryName);
    alert("Loading All Top Products For Category" + categoryName);
    let result = service.pushTopProductsByCategory(['12','13','14','15','16','17','18','19','20']);
    result.subscribe((result) => {
      console.log(result);
    })

  }

  fireView(object, service: ServicesAliexpressService){
    let wantedView = object.target.id;
    let categoryName = object.srcElement.parentElement.parentNode.cells[0].innerText; // i dont even need to find anything else is this they best way of doing it?
    console.log("Firing Push for Object " + "id: " + wantedView + ", categoryName: " + categoryName);
    console.log(categoryName);
    alert("Loading All Top Products For Category" + categoryName);
    this.rows = this.productCategories.length / 4;

  }

  over($event){
    // $('[data-toggle="tooltip"]').tooltip()
  }
  out($event){

    alert($event);
  }

  categoryHover($event){}
  totalProductsHover($event){}
  firePull(){}

  private findFirst(id){
    let i = this.productCategories.length;
    alert("Finding Product Category Name");
  }
  receiveSearchEvent($event){
    console.log("receiveSearchEvent  " + $event);

    if ($event === 'xvfg34r4'){ // security feature to prevent code injection, for every input output element
      // TODO: call api again to update categories Object
      this.productCategories = [
          {categoryName: "Kitchen", categoryId: 13, totalProducts: 100, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Tables", categoryId: 14, totalProducts: 200, lastFetched: "Yesterday", lastPushed: "Never" },
          {categoryName: "Computer", categoryId: 15, totalProducts: 300, lastFetched: "4h ago", lastPushed: "3h ago"},
          {categoryName: "Test4", categoryId: 16, totalProducts: 400, lastFetched: "20m ago", lastPushed: "20m ago"},
          {categoryName: "Test5", categoryId: 17, totalProducts: 500, lastFetched: "6h ago", lastPushed: "6h ago"},
          {categoryName: "Test6", categoryId: 18, totalProducts: 600, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test7", categoryId: 19, totalProducts: 700, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test8", categoryId: 20, totalProducts: 800, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test9", categoryId: 21, totalProducts: 900, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test10", categoryId: 22, totalProducts:1000, lastFetched: "Never", lastPushed: "Never"},
          {categoryName: "Test11", categoryId: 23, totalProducts:1100, lastFetched: "Never", lastPushed: "Never"}
      ]
    }else{
    //update this.productCategories Array upon search, recall the api to reget the datalist
    this.productCategories = [
      {categoryName: "Kitchen", categoryId: 13, totalProducts: 100, lastFetched: "Never", lastPushed: "Never"},
      {categoryName: "Tables", categoryId: 14, totalProducts: 200, lastFetched: "Yesterday", lastPushed: "Never" },
      {categoryName: "Computer", categoryId: 15, totalProducts: 300, lastFetched: "4h ago", lastPushed: "3h ago"},
      {categoryName: "Test4", categoryId: 16, totalProducts: 400, lastFetched: "20m ago", lastPushed: "20m ago"},
      {categoryName: "Test5", categoryId: 17, totalProducts: 500, lastFetched: "6h ago", lastPushed: "6h ago"}
    ]
  }
}
}
