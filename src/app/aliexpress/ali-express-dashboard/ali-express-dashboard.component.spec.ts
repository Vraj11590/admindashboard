import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AliExpressDashboardComponent } from './ali-express-dashboard.component';

describe('AliExpressDashboardComponent', () => {
  let component: AliExpressDashboardComponent;
  let fixture: ComponentFixture<AliExpressDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AliExpressDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AliExpressDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
