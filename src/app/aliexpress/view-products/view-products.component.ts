import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'



@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.css']
})
export class ViewProductsComponent implements OnInit {

  products: Array<object>;
  ngOnInit() {
  }


  constructor(  private route: ActivatedRoute, private router: Router){
    this.products = [{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },
    {
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    },{
        productTitle: "classic best-selling cream colored kitchen cabinets(LH-SW066)",
        volume: 313,
        productId: 32356288255,
        salePrice: 1444.00,
        imageUrl: "https://ae01.alicdn.com/kf/HTB1liLyHVXXXXbhXXXXq6xXFXXXw/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066-.jpg",
        validTime: "2018-12-14",
        productUrl: "https://www.aliexpress.com/item/classic-best-selling-cream-colored-kitchen-cabinets-LH-SW066/32356288255.html",
        localPrice: 1444.00
    }]



  }



}
