import { TestBed } from '@angular/core/testing';

import { ServicesAliexpressService } from './services-aliexpress.service';

describe('ServicesAliexpressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicesAliexpressService = TestBed.get(ServicesAliexpressService);
    expect(service).toBeTruthy();
  });

  it('should return an array of categoryId\'s with pushed status', () => {
    const service: ServicesAliexpressService = TestBed.get(ServicesAliexpressService);
    let categoriesArray = ['12', '13', '14', '15', '16', '17', '18', '19', '20'];
    expect(service.pushTopProductsByCategory(categoriesArray)).toContain(Array);

  })


});
