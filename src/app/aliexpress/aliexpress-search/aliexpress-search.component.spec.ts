import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AliexpressSearchComponent } from './aliexpress-search.component';

describe('AliexpressSearchComponent', () => {
  let component: AliExpressSearchComponent;
  let fixture: ComponentFixture<AliexpressSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AliexpressSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AliexpressSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
