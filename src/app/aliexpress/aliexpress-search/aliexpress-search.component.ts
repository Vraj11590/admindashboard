import { Component, Output, EventEmitter } from '@angular/core';

import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-aliexpress-search',
  templateUrl: './aliexpress-search.component.html',
  styleUrls: ['./aliexpress-search.component.css']
})
export class AliExpressSearchComponent{

  @Output() searchEvent = new EventEmitter<string>();


  constructor() {

    //
    // this.search => (event){
    //   alert(event)
    // }


  }
  ngOnInit() {
    console.log("Search initiliazed on search..");
    // $('#productSearch').attr("placeholder", "test");




  }
  searchFunction($event){

    let emitSearchTimeOut = null;
    clearTimeout(emitSearchTimeOut);
    let searchString = $event.target.value;
    // const searchInput = document.getElementById('productSearch');
    // const source = fromEvent(searchInput, 'input').pipe(
    //   map( (e: Event) => (<Event>e).target.value),
    //   // filter( text => text.length > 2),
    //   debounceTime(10),
    //   distinctUntilChanged(),
    //   switchMap( (e) => {
    //     console.log(e)
    //   })
    //
    // );

    emitSearchTimeOut = setTimeout( () => {
      if( searchString.length > 2){
        this.searchEvent.emit(searchString);
      }
      if( searchString.length == 0){
        this.searchEvent.emit("xvfg34r4");
      }
    }, 2000)

    // alert("event fired");
  }
  searchFunctionGo(test){
    alert("event fired");
  }
}
