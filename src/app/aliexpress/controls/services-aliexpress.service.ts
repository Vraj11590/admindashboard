import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as MyGlobals from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ServicesAliexpressService {
  constructor(private http: HttpClient) {
    let mainCategoriesUrl = MyGlobals.api+'getMainProductCategories';  // URL to web api
    let subCategoriesUrl = MyGlobals.api+'getAllProductCategoryData';
  }
  getMainProductCategories(){
    return this.http.get(MyGlobals.api+'getAllProductCategories');
  }
  pushTopProductsByCategory(categoryIds){
    return this.http.post(MyGlobals.pushTopProductsByCategory, categoryIds);
  }
}
