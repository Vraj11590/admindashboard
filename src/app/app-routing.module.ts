import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DashboardModule } from './dashboard/dashboard.module';
import { AliExpressModule } from './aliexpress/aliexpress.module';


const routes: Routes = [
  {
    path: '',
    redirectTo:'/aliexpress',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'aliexpress',
    loadChildren: './aliexpress/aliexpress.module#AliExpressModule'
  },
  {
    path: 'aliexpress/category/:categoryId',
    loadChildren: './aliexpress/aliexpress.module#AliExpressModule'
  },
  {
    path: 'aliexpress/products/:productId',
    loadChildren: './aliexpress/aliexpress.module#AliExpressModule'
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
